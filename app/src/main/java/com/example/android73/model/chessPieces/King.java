package com.example.android73.model.chessPieces;

import com.example.android73.R;
import com.example.android73.model.Player;

import java.io.Serializable;

/**
 * This class extends ChessPiece and represents the King pieces on the Board.
 * It contains constructors to create a King object by providing an x position, a y position, and a playerCode
 * and to create a King object by providing another King object (to duplicate the provided King object).
 * This class also contains logic to determine whether a provided x and y position is valid for 
 * two given Player objects representing the player the King belongs to and that player's opponent as well as
 * checking whether the King can castle and whether a passed move will put the King in check.
 * 
 * @author Rob Holda 
 * @author Nick LaBoy
 */
public class King extends ChessPiece implements Serializable {
	
	/**
	 * Initializes the King based on the x position, y position, and playerCode provided.
	 * 
	 * @param xPos The x position at which to create the King.
	 * @param yPos The y position at which to create the King.
	 * @param playerCode The playerCode representing the Player object the King belongs to.
	 */
	public King(int xPos, int yPos, String playerCode) {
		this.pieceCode = KING_CODE;
		this.playerCode = playerCode;
		this.xPos = xPos;
		this.yPos = yPos;

		if(playerCode == Player.WHITE_CODE) {
			pieceImageID = R.drawable.whiteking;
		} else {
			pieceImageID = R.drawable.blackking;
		}
}
	
	/**
	 * Initializes the King based on the provided King, essentially creating a 
	 * duplicate of the King passed in the king argument.
	 * 
	 * @param king The King object to duplicate.
	 */
	public King (King king) {
		this.pieceCode = king.pieceCode;
		this.playerCode = king.playerCode;
		this.xPos = king.xPos;
		this.yPos = king.yPos;
		this.numTimesMoved = king.numTimesMoved;
	}
	
	/**
	 * Determines whether the King can castle. This is currently based entirely upon the
	 * number of times it was previously moved (if the King has never been moved, it can
	 * castle.
	 * 
	 * @return true if the King has not been moved yet, otherwise false.
	 */
	public boolean canCastle() {
		return this.numTimesMoved == 0;
	}

	@Override
	public boolean canMoveToNewPosition(int xPosition, int yPosition, char promotionCode, Player me, Player them) {
		int minY = Math.min(this.yPos, yPosition);
		int maxY = Math.max(this.yPos, yPosition);
		int minX = Math.min(this.xPos, xPosition);
		int maxX = Math.max(this.xPos, xPosition);
		
		int yMagnitude = (maxY - minY);
		int xMagnitude = (maxX - minX);		
		
		// if attempting to castle and the king -can- castle
		if(this.canCastle() && yPosition == this.yPos && xMagnitude == 2) {
			boolean lookAtLeftRook = xPosition < this.xPos;
			boolean piecesBetween = lookAtLeftRook ? me.pieceAtPosition(this.xPos - 1, this.yPos) != null ||
					me.pieceAtPosition(this.xPos - 2, this.yPos) != null ||
					me.pieceAtPosition(this.xPos - 3, this.yPos) != null : me.pieceAtPosition(this.xPos + 1, this.yPos) != null ||
							me.pieceAtPosition(this.xPos + 2, this.yPos) != null;
			
			int myStepMultiplier = lookAtLeftRook ? -1 : 1;
			int potentialRookFile = lookAtLeftRook ? 0 : 7;
			ChessPiece pieceToLookAt = me.pieceAtPosition(potentialRookFile, this.yPos);
			
			// if no piece at position we're expecting or it's not a rook, invalid move
			if(pieceToLookAt == null || !(pieceToLookAt instanceof Rook)) {
				return false;
			}
			
			Rook castlingRook = (Rook)pieceToLookAt;
			
			// if the rook -can- castle
			if(castlingRook.canCastle()) {
				// if there's pieces between the king and rook, castling is invalid.
				if(piecesBetween) {
					return false;
				}
				
				// if the king is in check, castling is invalid.
				if(me.checkForCheck(them)) {
					return false;
				}
				Player simPlayer = new Player(me);
				King myKing = me.getMyKing();
				simPlayer.getMyKing().moveToPosition(myKing.xPos + (1 * myStepMultiplier), yPosition);
				
				// if the space one to the right or left will put the king in check, castling is invalid.
				if(simPlayer.checkForCheck(them)) {
					return false;
				}
				
				// if the space two to the right or left will put the king in check, castling is invalid.
				simPlayer.getMyKing().moveToPosition(myKing.xPos + (2 * myStepMultiplier), yPosition);
				if(simPlayer.checkForCheck(them)) {
					return false;
				}
			} else {
				return false;
			}
			
			return true;
		}
		
		// if not castling and our move moves us more than one space in any direction, it is invalid.
		if(xMagnitude > 1 || yMagnitude > 1) {
			return false;
		}
		
		// Since Kings can move any way that Rooks and Bishops can (limited to one in either or
		// both directions, now that we've ruled out castling and moves of more than 1
		// in either direction, we can just create a 
		// fake Bishop and a fake Rook for the current player at the King's current 
		// position and check if the move is valid for either of these.
		Bishop b = new Bishop(this.xPos, this.yPos, this.playerCode);
		Rook r = new Rook(this.xPos, this.yPos, this.playerCode);
				
		if(b.canMoveToNewPosition(xPosition, yPosition, promotionCode, me, them) ||
			r.canMoveToNewPosition(xPosition, yPosition, promotionCode, me, them)) {
			if(this.doesMovingKingHereCauseCheck(me, them, xPosition, yPosition)) {
				return false;
			} else {
				return true;				
			}
		}
		
		return false;
	}
	
	/**
	 * Determines whether the King moving to the passed x and y position puts the King 
	 * in check based on the passed Player object it belongs to 
	 * (in the me parameter) and the passed opposition
	 * Player object (in the them parameter) and returns the boolean result
	 * of this check. 
	 * 
	 * @param me The Player object the King belongs to.
	 * @param them The Player object of the opposing 
	 * @param xPosition The x position to try moving the King to.
	 * @param yPosition The y position to try moving the King to.
	 * 
	 * @return true if moving the King to the x and y position passed 
	 * (in the xPosition and yPosition parameters respectively) doesn't
	 * put it in check, otherwise returns false. 
	 */
	public boolean doesMovingKingHereCauseCheck(Player me, Player them, int xPosition, int yPosition) {
		Player simPlayer = new Player(me);
		Player simThem = new Player(them);
		
		simPlayer.getMyKing().moveToPosition(xPosition, yPosition);
		simThem.killPieceAtPosition(xPosition, yPosition);
		
		if(simPlayer.checkForCheck(simThem)) {
			return true;
		}
		
		return false;
	}

	public boolean isEnPassant(int xPosition, int yPosition, Player me, Player them) {
		return false;
	}

	public boolean isCastling(int xPosition, int yPosition, Player me, Player them) {
		int minY = Math.min(this.yPos, yPosition);
		int maxY = Math.max(this.yPos, yPosition);
		int minX = Math.min(this.xPos, xPosition);
		int maxX = Math.max(this.xPos, xPosition);

		int yMagnitude = (maxY - minY);
		int xMagnitude = (maxX - minX);

		// can assume that if we're checking this, it's a valid castling move
		if(yMagnitude == 0 && xMagnitude > 1 && canCastle()) {
			return true;
		}

		return false;
	}

	public boolean isPromotion(int xPosition, int yPosition, Player me, Player them) {
		return false;
	}

	public Rook getCastlingTargetRook(int xPosition, int yPosition, Player me, Player them) {
		int minY = Math.min(this.yPos, yPosition);
		int maxY = Math.max(this.yPos, yPosition);
		int minX = Math.min(this.xPos, xPosition);
		int maxX = Math.max(this.xPos, xPosition);

		int xMagnitude = (maxX - minX);
		boolean lookAtLeftRook = xPosition < this.xPos;
		int potentialRookFile = lookAtLeftRook ? 0 : 7;
		return (Rook) me.pieceAtPosition(potentialRookFile, this.yPos);
	}

	@Override
	public Pawn undoPromotion() {
		// this should never happen
		return null;
	}
}
