package com.example.android73.model.chessPieces;

import android.app.Activity;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;

import com.example.android73.model.Player;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

/**
 * Abstract ChessPiece class which is extended by all types of ChessPiece
 * (Pawn, Rook, Knight, Bishop, Queen, and King). Contains constants and fields 
 * all types of ChessPiece objects use, abstract methods all types of ChessPiece
 * objects will need to implement, and implementations of methods for logic that 
 * is the same for all types of ChessPiece objects.
 * 
 * @author Rob Holda 
 * @author Nick LaBoy
 *
 */
public abstract class ChessPiece implements Serializable {
	
	/** The single character used to represent the identity of the Player 
	 * object the ChessPiece belongs to. This is used in determining the validity
	 * of moves in general and attempts to promote a Pawn as well as actually
	 * carrying out a move. This is also used with the ChessPiece's pieceCode
	 * to visually represent the ChessPiece when the Board is drawn.*/
	public String playerCode;
	
	/** The single character used to represent the type of ChessPiece the 
	 * ChessPiece object it belongs to is. This is used with the ChessPiece's playerCode
	 * to visually represent the ChessPiece when the Board is drawn. */
	public String pieceCode;
	
	/** The piece's current file and it's current x position on the board. Used
	 * to determine where to display the piece as well as in calculations when 
	 * both moving the piece and determining the validity of both players' moves.*/
	public int xPos;
	
	/** The piece's current rank and it's current y position on the board. Used
	 * to determine where to display the piece as well as in calculations when 
	 * both moving the piece and determining the validity of both players' moves.*/
	public int yPos;
	
	/** This counter is updated every time a piece moves and is used as part of the process
	 * to determine whether a King+Rook can castle or a Pawn can en passant.*/
	public int numTimesMoved;

	/** The Image used to display the piece to the screen.*/
	public int pieceImageID;

	/** The pieceCode used to represent Pawns. Used to visually represent the ChessPiece.*/
	public static final String PAWN_CODE = "p";
	/** The pieceCode used to represent Rooks. Used both to visually represent the ChessPiece
	 * and to determine if an attempt to promote a Pawn to another piece is valid.*/
	public static final String ROOK_CODE = "R";
	/** The pieceCode used to represent Knights. Used both to visually represent the ChessPiece
	 * and to determine if an attempt to promote a Pawn to another piece is valid.*/
	public static final String KNIGHT_CODE = "N";
	/** The pieceCode used to represent Bishops. Used both to visually represent the ChessPiece
	 * and to determine if an attempt to promote a Pawn to another piece is valid.*/
	public static final String BISHOP_CODE = "B";
	/** The pieceCode used to represent Queens. Used both to visually represent the ChessPiece
	 * and to determine if an attempt to promote a Pawn to another piece is valid.*/
	public static final String QUEEN_CODE = "Q";
	/** The pieceCode used to represent Kings. Used to visually represent the ChessPiece.*/
	public static final String KING_CODE = "K";

	/**
	 * Determines whether or not the move represented by the parameters
	 * provided is legal and returns the boolean result of these checks.
	 * 
	 * @param xPosition The x position / file to check the validity of 
	 * moving to for the ChessPiece.
	 * @param yPosition The y position / rank to check the validity of 
	 * moving to for the ChessPiece.
	 * @param promotionCode The promotionCode used to check the validity of
	 * moving the ChessPiece. This only comes into play on Pawn's 
	 * Override of this function.
	 * @param me The Player object to which the ChessPiece belongs. Used to 
	 * determine the validity of the move.
	 * @param them The opposing Player object. Used to 
	 * determine the validity of the move.
	 * @return true if the move represented by the parameters provided is 
	 * legal and false otherwise.
	 */
	public abstract boolean canMoveToNewPosition(int xPosition, int yPosition, char promotionCode, Player me, Player them);

	public abstract boolean isEnPassant(int xPosition, int yPosition, Player me, Player them);
	public abstract boolean isCastling(int xPosition, int yPosition, Player me, Player them);
	public abstract boolean isPromotion(int xPosition, int yPosition, Player me, Player them);
	public abstract Pawn undoPromotion();

	/**
	 * Determines whether or not the ChessPiece is at the position passed by
	 * the caller and returns the boolean result of this check.
	 * 
	 * @param xPosition The x position / file to check against for the 
	 * ChessPiece.
	 * @param yPosition The y position / rank to check against for the 
	 * ChessPiece.
	 * @return true if the ChessPiece is at the position passed to the method and false
	 * if it is not.
	 */
	public boolean isAtPosition(int xPosition, int yPosition) {
		return this.xPos == xPosition && this.yPos == yPosition;
	}
	
	/**
	 * Returns the string used to identify the ChessPiece on the Board which
	 * is a combination of the ChessPiece's playerCode and pieceCode.
	 */
	public String toString() {
		return playerCode + pieceCode;
	}
	
	/**
	 * Moves the ChessPiece to the position passed to the method
	 * and updates the number of times the piece has been moved.
	 * 
	 * @param xPosition The x position / file to move the 
	 * ChessPiece to.
	 * @param yPosition The y position / rank to move the 
	 * ChessPiece to.
	 */
	public void moveToPosition(int xPosition, int yPosition) {

		this.xPos = xPosition;
		this.yPos = yPosition;
		this.numTimesMoved++;
	}

	public static char parseIndexIntoFile(int index) {
		if(index == 0) { return 'a'; }
		if(index == 1) { return 'b'; }
		if(index == 2) { return 'c'; }
		if(index == 3) { return 'd'; }
		if(index == 4) { return 'e'; }
		if(index == 5) { return 'f'; }
		if(index == 6) { return 'g'; }
		if(index == 7) { return 'h'; }

		return ' ';
	}
}
