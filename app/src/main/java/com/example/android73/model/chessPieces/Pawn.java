package com.example.android73.model.chessPieces;

import android.content.res.AssetManager;
import android.graphics.BitmapFactory;
import android.media.Image;

import com.example.android73.R;
import com.example.android73.model.Player;

import java.io.Serializable;

/**
 * This class extends ChessPiece and represents the Pawn pieces on the Board.
 * It contains constructors to create a Pawn object by providing an x position, a y position, and a playerCode
 * and to create a Pawn object by providing another Pawn object (to duplicate the provided Pawn object).
 * This class also contains logic to determine whether a provided x and y position is valid for 
 * two given Player objects representing the player the Pawn belongs to and that player's opponent
 * as well as logic to promote the Pawn (if necessary and valid to do so).
 * 
 * @author Rob Holda 
 * @author Nick LaBoy
 */
public class Pawn extends ChessPiece implements Serializable {
	
	/** The boolean flag used to store and determine whether the Pawn can be
	 * captured when by an opposing Player object's pawn en passant.*/
	public boolean canBeEnPassanted;
	
	/**
	 * Initializes the Pawn based on the x position, y position, and playerCode provided.
	 * 
	 * @param xPos The x position at which to create the Pawn.
	 * @param yPos The y position at which to create the Pawn.
	 * @param playerCode The playerCode representing the Player object the Pawn belongs to.
	 */
	public Pawn(int xPos, int yPos, String playerCode) {
		this.pieceCode = PAWN_CODE;
		this.playerCode = playerCode;
		this.xPos = xPos;
		this.yPos = yPos;
		this.canBeEnPassanted = false;

		if(playerCode == Player.WHITE_CODE) {
			pieceImageID = R.drawable.whitepawn;
		} else {
			pieceImageID = R.drawable.blackpawn;
		}
	}
	
	/**
	 * Initializes the Pawn based on the provided Pawn, essentially creating a 
	 * duplicate of the Pawn passed in the pawn argument.
	 * 
	 * @param pawn The Pawn object to duplicate.
	 */
	public Pawn (Pawn pawn) {
		this.pieceCode = pawn.pieceCode;
		this.playerCode = pawn.playerCode;
		this.xPos = pawn.xPos;
		this.yPos = pawn.yPos;
		this.numTimesMoved = pawn.numTimesMoved;
		
		this.canBeEnPassanted = pawn.canBeEnPassanted;
	}
	
	public void moveToPosition(int xPosition, int yPosition) {
		if(this instanceof Pawn) {
			int xMag = Math.abs(xPosition - this.xPos);
			int yMag = Math.abs(yPosition - this.yPos);
			if(this.numTimesMoved == 0 && xMag == 0 && yMag == 2) {
				((Pawn)this).canBeEnPassanted = true;
			}
		}
		
		super.moveToPosition(xPosition, yPosition);
	}
	
	/**
	 * Determines if the Pawn is at the right y position to be promoted
	 * for the Player object it belongs to. If not, returns itself, otherwise
	 * returns the result of a call to actuallyPromotePawn which actually handles 
	 * the promotion itself.
	 * 
	 * @param promotionCode The pieceCode value of the new ChessPiece 
	 * child type we're promoting the Pawn to (assuming it's valid to promote
	 * the Pawn.
	 * 
	 * @return Either the Pawn itself if promotion is invalid for the Pawn's playerCode 
	 * and y position, or the new post-promotion ChessPiece child type object to
	 * replace the Pawn. 
	 */
	public ChessPiece promoteIfNecessary (String promotionCode) {
		ChessPiece newSelf = this;
		
		if(this.playerCode.equals(Player.BLACK_CODE)) {
			if(this.yPos == 1) {
				newSelf = this.actuallyPromotePawn(promotionCode);
			}
		} else {
			if(this.yPos == 8) {
				newSelf = this.actuallyPromotePawn(promotionCode);
			}
		}
		
		return newSelf;
	}

	/**
	 * Actually handles the promotion of the Pawn either replacing it
	 * with a new ChessPiece of the subtype corresponding to the 
	 * pieceCode value provided in the promotionCode parameter or
	 * the Pawn itself if this pieceCode is invalid.
	 * 
	 * @param promotionCode The pieceCode value of the new ChessPiece 
	 * child type we're promoting the Pawn to.
	 * 
	 * @return Either the Pawn itself if promotion is invalid for the pieceCode
	 * value provided in the promotionCode parameter or the new post-promotion 
	 * ChessPiece child type object to replace the Pawn. 
	 */
	public ChessPiece actuallyPromotePawn(String promotionCode) {
		ChessPiece newSelf = this;
		if(promotionCode.equals(" ") || promotionCode.equals(ChessPiece.QUEEN_CODE)) {
			newSelf = new Queen(this.xPos, this.yPos, this.playerCode);
			newSelf.numTimesMoved = this.numTimesMoved;
		} else if(promotionCode.equals(ChessPiece.ROOK_CODE)) {
			newSelf = new Rook(this.xPos, this.yPos, this.playerCode);
			newSelf.numTimesMoved = this.numTimesMoved;
		} else if(promotionCode.equals(ChessPiece.KNIGHT_CODE)) {
			newSelf = new Knight(this.xPos, this.yPos, this.playerCode);
			newSelf.numTimesMoved = this.numTimesMoved;
		} else if(promotionCode.equals(ChessPiece.BISHOP_CODE)) {
			newSelf = new Bishop(this.xPos, this.yPos, this.playerCode);
			newSelf.numTimesMoved = this.numTimesMoved;
		}
		
		return newSelf;
	}

	@Override
	public Pawn undoPromotion() {
		// this should never happen
		return null;
	}

	@Override
	public boolean canMoveToNewPosition(int xPosition, int yPosition, char promotionCode, Player me, Player them) {
		int movementMultiplier = this.playerCode == Player.BLACK_CODE ? -1 : 1;
		
		// check for own piece at dest location
		if(me.pieceAtPosition(xPosition, yPosition) != null) {
			return false;
		}
		
		// standard vertical move
		if(xPosition == this.xPos) {
			// one space movement
			if((yPosition - this.yPos) * movementMultiplier == 1) {
				return them.pieceAtPosition(xPosition, yPosition) == null;
			}
			
			// two space movement on first movement
			if((yPosition - this.yPos) * movementMultiplier == 2) {
				if(this.numTimesMoved == 0) {
					
					//no enemy piece directly in the spot to move to
					return 	them.pieceAtPosition(xPosition, yPosition) == null &&
							//no enemy piece obstructing our path
							them.pieceAtPosition(xPosition, yPosition - (1 * movementMultiplier)) == null &&
							//no piece of our own to obstruct our path
							me.pieceAtPosition(xPosition, yPosition - (1 * movementMultiplier)) == null;
				}
			}
		}
		
		// diagonal movement to capture a piece
		if((yPosition - this.yPos) * movementMultiplier  == 1 && Math.abs(this.xPos - xPosition) == 1) {
			ChessPiece theirPiece = them.pieceAtPosition(xPosition, yPosition);
			
			if(theirPiece != null) {
				return true;
			} else {
				ChessPiece enemyPiece = them.pieceAtPosition(xPosition, yPosition - movementMultiplier);
				
				// checking for en passant
				// if the enemy piece we're moving past is a pawn
				if(enemyPiece != null && enemyPiece instanceof Pawn) {
					Pawn enemyPawn = (Pawn)enemyPiece;
					// if this enemy's pawn can be en passanted 
					return enemyPawn.canBeEnPassanted;				
				}
			}
		}
		
		return false;
	}

	public boolean isEnPassant(int xPosition, int yPosition, Player me, Player them) {
		int movementMultiplier = this.playerCode == Player.BLACK_CODE ? -1 : 1;

		// diagonal movement to capture a piece
		if((yPosition - this.yPos) * movementMultiplier  == 1 && Math.abs(this.xPos - xPosition) == 1) {
			ChessPiece theirPiece = them.pieceAtPosition(xPosition, yPosition);

			if(theirPiece == null) {
				ChessPiece enemyPiece = them.pieceAtPosition(xPosition, yPosition - movementMultiplier);

				// checking for en passant
				// if the enemy piece we're moving past is a pawn
				if(enemyPiece != null && enemyPiece instanceof Pawn) {
					Pawn enemyPawn = (Pawn)enemyPiece;
					// if this enemy's pawn can be en passanted
					return enemyPawn.canBeEnPassanted;
				}
			}
		}

		return false;
	}

	public Pawn getEnPassantTarget(int xPosition, int yPosition, Player me, Player them) {
		int movementMultiplier = this.playerCode == Player.BLACK_CODE ? -1 : 1;

		// diagonal movement to capture a piece
		if((yPosition - this.yPos) * movementMultiplier  == 1 && Math.abs(this.xPos - xPosition) == 1) {
			ChessPiece theirPiece = them.pieceAtPosition(xPosition, yPosition);

			if(theirPiece == null) {
				ChessPiece enemyPiece = them.pieceAtPosition(xPosition, yPosition - movementMultiplier);

				// checking for en passant
				// if the enemy piece we're moving past is a pawn
				if(enemyPiece != null && enemyPiece instanceof Pawn) {
					Pawn enemyPawn = (Pawn)enemyPiece;
					if(enemyPawn.canBeEnPassanted) {
						return enemyPawn;
					}
				}
			}
		}

		return null;
	}

	public boolean isCastling(int xPosition, int yPosition, Player me, Player them) {
		return false;
	}

	public boolean isPromotion(int xPosition, int yPosition, Player me, Player them) {
		if(this.playerCode.equals(Player.BLACK_CODE)) {
			if(this.yPos == 2) {
				return true;
			}
		} else {
			if(this.yPos == 7) {
				return true;
			}
		}

		return false;
	}
}
