package com.example.android73.model.chessPieces;

import com.example.android73.R;
import com.example.android73.model.Player;

import java.io.Serializable;

/**
 * This class extends ChessPiece and represents the Queen pieces on the Board.
 * It contains constructors to create a Queen object by providing an x position, a y position, and a playerCode
 * and to create a Queen object by providing another Queen object (to duplicate the provided Queen object).
 * This class also contains logic to determine whether a provided x and y position is valid for 
 * two given Player objects representing the player the Queen belongs to and that player's opponent.
 * 
 * @author Rob Holda 
 * @author Nick LaBoy
 */
public class Queen extends ChessPiece implements Serializable {
	
	/**
	 * Initializes the Queen based on the x position, y position, and playerCode provided.
	 * 
	 * @param xPos The x position at which to create the Queen.
	 * @param yPos The y position at which to create the Queen.
	 * @param playerCode The playerCode representing the Player object the Queen belongs to.
	 */
	public Queen(int xPos, int yPos, String playerCode) {
		this.pieceCode = QUEEN_CODE;
		this.playerCode = playerCode;
		this.xPos = xPos;
		this.yPos = yPos;

		if(playerCode == Player.WHITE_CODE) {
			pieceImageID = R.drawable.whitequeen;
		} else {
			pieceImageID = R.drawable.blackqueen;
		}
}
	
	/**
	 * Initializes the Queen based on the provided Queen, essentially creating a 
	 * duplicate of the Queen passed in the queen argument.
	 * 
	 * @param queen The Queen object to duplicate.
	 */
	public Queen (Queen queen) {
		this.pieceCode = queen.pieceCode;
		this.playerCode = queen.playerCode;
		this.xPos = queen.xPos;
		this.yPos = queen.yPos;
		this.numTimesMoved = queen.numTimesMoved;
	}

	@Override
	public boolean canMoveToNewPosition(int xPosition, int yPosition, char promotionCode, Player me, Player them) {
		
		// Since Queens can move any way that Rooks and Bishops can (not including castling
		// which will be handled by King) we can just create a fake Bishop and a fake Rook 
		// for the current player at the Queen's current position and check if the move
		// is valid for either of these.
		Bishop b = new Bishop(this.xPos, this.yPos, this.playerCode);
		Rook r = new Rook(this.xPos, this.yPos, this.playerCode);
		
		if(b.canMoveToNewPosition(xPosition, yPosition, promotionCode, me, them) ||
				r.canMoveToNewPosition(xPosition, yPosition, promotionCode, me, them)) {
			return true;
		}
		
		return false;
	}

	public boolean isEnPassant(int xPosition, int yPosition, Player me, Player them) {
		return false;
	}

	public boolean isCastling(int xPosition, int yPosition, Player me, Player them) {
		return false;
	}

	public boolean isPromotion(int xPosition, int yPosition, Player me, Player them) {
		return false;
	}

	@Override
	public Pawn undoPromotion() {
		Pawn oldSelf = new Pawn(this.xPos, this.yPos, this.playerCode);
		oldSelf.numTimesMoved = this.numTimesMoved;
		return oldSelf;
	}
}
