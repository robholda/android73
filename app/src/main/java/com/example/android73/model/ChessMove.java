package com.example.android73.model;

import com.example.android73.model.chessPieces.ChessPiece;

import java.io.Serializable;

public class ChessMove implements Serializable {


    public String startPosition;
    public String endPosition;
    public String playerCode;
    public String playerName;
    public ChessPiece takenPiece;
    public String promotionCode;

    public ChessPiece castledRook;

    public int output;

    public boolean isEnPassant;
    public boolean isCastling;
    public boolean isPromotion;
    public boolean playerTappedDraw;

    public static final int NO_OUTPUT = 0;
    public static final int CHECK_OUTPUT = 1;
    public static final int CHECKMATE_OUTPUT = 2;
    public static final int STALEMATE_OUTPUT = 3;
    public static final int PROMOTION_OUTPUT = 4;
    public static final int RESIGNATION_OUTPUT = 5;
    public static final int DRAW_OUTPUT = 6;

    public ChessMove(String playerCode, String playerName) {
        this.playerCode = playerCode;
        this.playerName = playerName;
    }

    public String toString() {
        return startPosition + " " + endPosition;
    }

    public boolean shouldPrintMove() {
        return output != CHECKMATE_OUTPUT && output != STALEMATE_OUTPUT && output != RESIGNATION_OUTPUT && output != DRAW_OUTPUT;
    }

    public String toOutputString() {

        if(output == NO_OUTPUT) {
            return "";
        } else if(output == PROMOTION_OUTPUT) {
            return "";
        } else if(output == CHECK_OUTPUT) {
            return "Check!";
        } else if(output == CHECKMATE_OUTPUT) {
            return "Checkmate! " + playerName + " wins!";
        } else if(output == STALEMATE_OUTPUT) {
            return "Stalemate!";
        } else if(output == DRAW_OUTPUT) {
            return "Draw";
        } else if(output == RESIGNATION_OUTPUT) {
            String them = playerName.equalsIgnoreCase(Player.whiteName) ? Player.blackName : Player.whiteName;
            return playerName + " Resigned! " + them + " Wins!";
        }

        return "";
    }

    public int getStartFile() {
        return parseFileIntoIndex(startPosition.charAt(0));
    }

    public int getStartRank() {
        return parseRankIntoIndex(startPosition.charAt(1));
    }

    public int getEndFile() {
        return parseFileIntoIndex(endPosition.charAt(0));
    }

    public int getEndRank() {
        return parseRankIntoIndex(endPosition.charAt(1));
    }

    /**
     * Maps the character provided to its corresponding valid file or -1 for invalid input and returns that mapped value.
     *
     * @param file The file character to parse.
     * @return The integer position on the Board corresponding to the provided file character or -1 for invalid characters.
     */
    public static int parseFileIntoIndex(char file) {
        if(file == 'a') { return 0; }
        if(file == 'b') { return 1; }
        if(file == 'c') { return 2; }
        if(file == 'd') { return 3; }
        if(file == 'e') { return 4; }
        if(file == 'f') { return 5; }
        if(file == 'g') { return 6; }
        if(file == 'h') { return 7; }

        return -1;
    }

    public static int parseRankIntoIndex(char rank) {
        return Character.getNumericValue(rank);
    }
}
