package com.example.android73.model.chessPieces;

import com.example.android73.R;
import com.example.android73.model.Player;

import java.io.Serializable;

/**
 * This class extends ChessPiece and represents the Knight pieces on the Board.
 * It contains constructors to create a Knight object by providing an x position, a y position, and a playerCode
 * and to create a Knight object by providing another Knight object (to duplicate the provided Knight object).
 * This class also contains logic to determine whether a provided x and y position is valid for 
 * two given Player objects representing the player the Knight belongs to and that player's opponent.
 * 
 * @author Rob Holda 
 * @author Nick LaBoy
 */
public class Knight extends ChessPiece implements Serializable {
	
	/**
	 * Initializes the Knight based on the x position, y position, and playerCode provided.
	 * 
	 * @param xPos The x position at which to create the Knight.
	 * @param yPos The y position at which to create the Knight.
	 * @param playerCode The playerCode representing the Player object the Knight belongs to.
	 */
	public Knight(int xPos, int yPos, String playerCode) {
		this.pieceCode = KNIGHT_CODE;
		this.playerCode = playerCode;
		this.xPos = xPos;
		this.yPos = yPos;

		if(playerCode == Player.WHITE_CODE) {
			pieceImageID = R.drawable.whiteknight;
		} else {
			pieceImageID = R.drawable.blackknight;
		}
}
	
	/**
	 * Initializes the Knight based on the provided Knight, essentially creating a 
	 * duplicate of the Knight passed in the knight argument.
	 * 
	 * @param knight The Knight object to duplicate.
	 */
	public Knight (Knight knight) {
		this.pieceCode = knight.pieceCode;
		this.playerCode = knight.playerCode;
		this.xPos = knight.xPos;
		this.yPos = knight.yPos;
		this.numTimesMoved = knight.numTimesMoved;
	}

	@Override
	public boolean canMoveToNewPosition(int xPosition, int yPosition, char promotionCode, Player me, Player them) {
		int minY = Math.min(this.yPos, yPosition);
		int maxY = Math.max(this.yPos, yPosition);
		int minX = Math.min(this.xPos, xPosition);
		int maxX = Math.max(this.xPos, xPosition);
		
		int yMagnitude = (maxY - minY);
		int xMagnitude = (maxX - minX);
		
		// move is only valid if it's 2 in one direction and 1 in the other
		if((yMagnitude == 2 && xMagnitude == 1) || (yMagnitude == 1 && xMagnitude == 2)) {
			// if I have a piece at the destination, this move is invalid.
			if(me.pieceAtPosition(xPosition, yPosition) != null) {
				return false;
			}
			
			return true;
		}
		
		return false;
	}

	public boolean isEnPassant(int xPosition, int yPosition, Player me, Player them) {
		return false;
	}

	public boolean isCastling(int xPosition, int yPosition, Player me, Player them) {
		return false;
	}

	public boolean isPromotion(int xPosition, int yPosition, Player me, Player them) {
		return false;
	}

    @Override
    public Pawn undoPromotion() {
        Pawn oldSelf = new Pawn(this.xPos, this.yPos, this.playerCode);
        oldSelf.numTimesMoved = this.numTimesMoved;
        return oldSelf;
    }
}
