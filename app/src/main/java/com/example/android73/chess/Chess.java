package com.example.android73.chess;

import java.util.Scanner;

import com.example.android73.model.Player;
import com.example.android73.model.chessPieces.ChessPiece;
import com.example.android73.model.chessPieces.Pawn;
import com.example.android73.view.Board;

/**
 * The main class for this program. The main method and input validation
 * and parsing occur here. 
 * 
 * @author Rob Holda 
 * @author Nick LaBoy
 */
public class Chess {
	/** The string to compare user input against to confirm resignation.*/
	public static String resignInputString = "resign";
	/** The string to compare user input against to confirm offering a draw,
	 * this must be appended to the end of the input used to signify a valid move.*/
	public static String offerDrawInputStringSuffix = "draw?";
	/** The string to compare user input against to confirm accepting a draw.*/
	public static String acceptDrawInputString = "draw";
	
	/** The integer used to signify that the move we've just made does not cause check, checkmate, or stalemate.*/
	public static int newStateContinue = 0;
	/**  The integer used to signify that the move we've just made puts the opponent in check.*/
	public static int newStateCheck = 1;
	/**  The integer used to signify that the move we've just made puts the opponent in checkmate.*/
	public static int newStateWin = 2;
	/**  The integer used to signify that the move we've just made causes a stalemate.*/
	public static int newStateDraw = 3;
	
	/**
	 * The main method. This is where the game's state, the actual reading of user input,
	 * and game loop happen.
	 * 
	 * @param args Command line args. Nothing is done with these in the case of our chess program.
	 */
//	public static void main(String[] args) {
//		System.out.println("");
//
//		// game state
//		boolean gameOver = false;
//		boolean whitesTurn = true;
//		boolean drawOffered = false;
//
//		// initialize game objects
//		Board b = new Board();
//		Player white = new Player(Player.WHITE_CODE);
//		Player black = new Player(Player.BLACK_CODE);
//
//		Player me = white;
//		Player them = black;
//
//		Scanner userInputScanner = new Scanner(System.in);
//		boolean printCheck = false;
//
//		// game loop
//		do {
//			// draw the board
//			b.drawBoard(black, white);
//
//			if(printCheck) {
//				System.out.println("Check\n");
//				printCheck = false;
//			}
//
//			// prompt for user input, looping until valid input is provided
//			String userInput;
//			boolean loopedAtLeastOnce = false;
//
//			do {
//				if(loopedAtLeastOnce) {
//					System.out.println("\nIllegal move, try again\n");
//				}
//				System.out.print(me.playerName + "'s move: ");
//				userInput = userInputScanner.nextLine();
//				loopedAtLeastOnce = true;
//
//			} while(!validateInput(userInput, drawOffered, me, them));
//
//			// handle resignation
//			if(userInput.equals(resignInputString)) {
//				System.out.println("\n" + them.playerName + " wins");
//				gameOver = true;
//				break;
//			}
//
//			if(drawOffered) {
//				// handle accepted draw
//				if(userInput.equals(acceptDrawInputString)) {
//					System.out.println("\ndraw");
//					gameOver = true;
//					break;
//				// handle rejected draw
//				} else {
//					drawOffered = false;
//				}
//			}
//
//			int stateChangeCausedByMove;
//
//			// handle draw offered
//			if(userInput.length() > 6 && userInput.substring(6).contentEquals(offerDrawInputStringSuffix)) {
//				// handle move
//				drawOffered = true;
//				stateChangeCausedByMove = actuallyHandleMove(me, them, userInput, true);
//			} else {
//				// actually do the move here.
//				stateChangeCausedByMove = actuallyHandleMove(me, them, userInput, true);
//			}
//
//			if(stateChangeCausedByMove == newStateWin) {
//				System.out.println(me.playerName + " wins");
//				gameOver = true;
//				break;
//			} else if(stateChangeCausedByMove == newStateDraw) {
//				System.out.println("\ndraw");
//				gameOver = true;
//				break;
//			} else {
//				if(stateChangeCausedByMove == newStateCheck) {
//					printCheck = true;
//				}
//				System.out.println("");
//			}
//
//			//switch users
//			whitesTurn = !whitesTurn;
//			me = whitesTurn ? white : black;
//			them = whitesTurn ? black : white;
//
//		} while(!gameOver);
//
//		userInputScanner.close();
//	}
	
	/**
	 * Parses the provided user input and has the Player object provided in the me parameter
	 * handle the parsed move.
	 * 
	 * @param me The Player object upon which to execute the move provided in the userInput parameter. 
	 * @param them The opposing Player object whom we will perform the move provided in the userInput parameter against.
	 * @param userInput The validated but unparsed String the user has provided which represents a move.
	 * @param logOutput The boolean that determines whether we print 'Check', 'Checkmate', 
	 * or 'Stalemate' to the console based on the occurrence of such states during this move.
	 * 
	 * @return The integer value associated with the result of the move defined by the parameters provided. This
	 * can be any of the values defined by the newStateCheck, newStateWin, newStateDraw, and newStateContinue members 
	 * defined on this class. These values are defined based upon whether the current move has caused the opponent
	 * Player object provided in the them parameter to enter check, checkmate, stalemate, or none of the above respectively.
	 */
	public static int actuallyHandleMove(Player me, Player them, String userInput, boolean logOutput) {
		int parsedMoveToFile = parseFileIntoIndex(userInput.charAt(3));
		int parsedMoveToRank = Character.getNumericValue(userInput.charAt(4));
		int parsedMoveFromFile = parseFileIntoIndex(userInput.charAt(0));
		int parsedMoveFromRank = Character.getNumericValue(userInput.charAt(1));
		String promotionCode = userInput.length() == 7 ? userInput.substring(6,7) : " ";
		
		return me.actuallyHandleMove(them, parsedMoveToFile, parsedMoveToRank, parsedMoveFromFile, parsedMoveFromRank, promotionCode, logOutput);
	}
	
	/**
	 * Determines if the character provided corresponds to a valid file and returns the boolean result of that check.
	 * 
	 * @param file The file character to validate.
	 * @return true if the character provided in the file parameter is any of the following: 'a', 'b', 'c', 'd', 'e', 'f', 'g', or 'h'.
	 * Otherwise, returns false.
	 */
	public static boolean validateFile(char file) {
		return (file == 'a' || file == 'b' || file == 'c' || file == 'd' || file == 'e' || file == 'f' || file == 'g' || file == 'h');
	}
	
	/**
	 * Determines if the character provided corresponds to a valid pieceCode of a ChessPiece child type to promote a Pawn
	 * to and returns the boolean result of that check.
	 * 
	 * @param newPieceCode The pieceCode to validate. 
	 * @return true if the character provided in the newPieceCode parameter is defined by any of the following:
	 * ROOK_CODE, KNIGHT_CODE, BISHOP_CODE, or QUEEN_CODE corresponding to the ChessPiece child types of
	 * Rook, Knight, Bishop, and Queen respectively. Otherwise returns false.
	 */
	public static boolean validatePromotionRequest(char newPieceCode) {
		return 	newPieceCode == ChessPiece.ROOK_CODE.charAt(0) || 
				newPieceCode == ChessPiece.KNIGHT_CODE.charAt(0) ||
				newPieceCode == ChessPiece.BISHOP_CODE.charAt(0) || 
				newPieceCode == ChessPiece.QUEEN_CODE.charAt(0);
	}
	
	/**
	 * Maps the character provided to its corresponding valid file or -1 for invalid input and returns that mapped value.
	 * 
	 * @param file The file character to parse.
	 * @return The integer position on the Board corresponding to the provided file character or -1 for invalid characters.
	 */
	public static int parseFileIntoIndex(char file) {
		if(file == 'a') { return 0; }
		if(file == 'b') { return 1; }
		if(file == 'c') { return 2; }
		if(file == 'd') { return 3; }
		if(file == 'e') { return 4; }
		if(file == 'f') { return 5; }
		if(file == 'g') { return 6; }
		if(file == 'h') { return 7; }
		
		return -1;
	}
	
	/**
	 * Determines if the character provided corresponds to a valid rank and returns the boolean result of that check.
	 * 
	 * @param rank The rank character to validate.
	 * @return true if the character provided in the rank parameter is any of the following: '1', '2', '3', '4', '5', '6', '7', or '8'.
	 * Otherwise, returns false.
	 */
	public static boolean validateRank(char rank) {
		return (rank == '1' || rank == '2' || rank == '3' || rank == '4' || rank == '5' || rank == '6' || rank == '7' || rank == '8');
	}
	
	
	/**
	 * Confirms whether the move defined in the userInput parameter is syntactically valid and is valid for the current game state
	 * (allowing accepting draw if draw has been offered). Also confirms whether an attempt to promote a Pawn is valid (based on the
	 * piece code provided in the userInput and the position to move the ChessPiece to and from also in the userInput parameter).
	 * 
	 * @param userInput The input provided by the user which represents their next move.
	 * @param drawOffered If this boolean value is true, userInput of 'draw' becomes a valid move which results in a draw. Otherwise
	 * 'draw' is an invalid move.
	 * @param me The Player object we wish to confirm the validity of executing the move provided in the userInput parameter upon. 
	 * @param them The opposing Player object we wish to confirm the validity of executing the move provided in the userInput parameter against.
	 * 
	 * @return true if the move defined in the userInput parameter is valid for the Player objects provided in the me and them parameters and the
	 * boolean state in the drawOffered parameter. False otherwise.
	 */
	public static boolean validateInput(String userInput, boolean drawOffered, Player me, Player them) {
		// validate resignation input
		if(userInput.equals(resignInputString)) {
			return true;
		// validate accept draw input
		} else if(userInput.equals(acceptDrawInputString)) {
			return drawOffered;
		}else {
			// validate actual input is in [A-H][1-8][space][A-H][1-8] format or
			//[A-H][1-8][space][A-H][1-8][space][draw?] or
			//[A-H][1-8][space][A-H][1-8][space][newPieceCode]
			if((userInput.length() != 5 && userInput.length() != 11 && userInput.length() != 7)) {
				return false;
			}
			
			// syntactically valid move
			if(userInput.length() == 5 && validateFile(userInput.charAt(0)) && validateRank(userInput.charAt(1)) && userInput.charAt(2) == ' ' &&
					validateFile(userInput.charAt(3)) && validateRank(userInput.charAt(4))) {
					return checkMoveWithValidatedInput(userInput, me, them);
			}
			
			// syntactically valid move including promotion code
			if(userInput.length() == 7 && validateFile(userInput.charAt(0)) && validateRank(userInput.charAt(1)) && userInput.charAt(2) == ' ' &&
					validateFile(userInput.charAt(3)) && validateRank(userInput.charAt(4)) && validatePromotionRequest(userInput.charAt(6))) {
					
					ChessPiece myPiece = me.pieceAtPosition(parseFileIntoIndex(userInput.charAt(0)), Character.getNumericValue(userInput.charAt(1)));
					
					// if trying to promote non-pawn piece, illegal move
					if(myPiece == null || !(myPiece instanceof Pawn)) {
						return false;
					}
					
					// if trying to promote pawn that's not promotable (it's not moving to a position 
					// that's a promotion position
					if((myPiece.playerCode.equals(Player.BLACK_CODE) && userInput.charAt(4) == '1') ||
							(myPiece.playerCode.equals(Player.WHITE_CODE) && userInput.charAt(4) == '8')) {
						return checkMoveWithValidatedInput(userInput, me, them);	
					} else {
						return false;
					}
			}
			
			// syntactically valid move offering draw
			if(userInput.length() == 11 && validateFile(userInput.charAt(0)) && validateRank(userInput.charAt(1)) && userInput.charAt(2) == ' ' &&
					validateFile(userInput.charAt(3)) && validateRank(userInput.charAt(4)) && userInput.charAt(5) == ' '
					&& userInput.substring(6).equals(offerDrawInputStringSuffix)) {
					return checkMoveWithValidatedInput(userInput, me, them);
			}
		}
		return false;
	}
	
	/**
	 * Confirms whether the move is actually valid. This includes checks to make sure that:
	 * -The move does not start and end at the same location
	 * -The Player object provided in the me parameter actually has a ChessPiece object to move at the location specified
	 * in the userInput parameter
	 * -The Player object provided in the me parameter does not already have a ChessPiece object located at the location
	 * the userInput parameter provides to move the piece to 
	 * -The move will not put the Player object provided in the me parameter into check
	 * -The move is actually valid for the piece located at the position provided in the userInput parameter
	 * 
	 * @param userInput The input provided by the user which represents their next move.
	 * @param me The Player object we wish to confirm the validity of executing the move provided in the userInput parameter upon. 
	 * @param them The opposing Player object we wish to confirm the validity of executing the move provided in the userInput parameter against.
	 * 
	 * @return true if the move defined in the userInput parameter is valid. Otherwise returns false.
	 */
	public static boolean checkMoveWithValidatedInput (String userInput, Player me, Player them) {
		ChessPiece pieceToMove = me.pieceAtPosition(parseFileIntoIndex(userInput.charAt(0)), Character.getNumericValue(userInput.charAt(1)));
		ChessPiece pieceAtPositionImMovingTo;
		
		// If our move starts and ends at the same position, it is invalid
		if(userInput.charAt(0) == userInput.charAt(3) && userInput.charAt(1) == userInput.charAt(4)) {
			return false;
		}
		
		// I don't have a piece at the starting position provided making this invalid
		if(pieceToMove == null) {
			return false;
		}
		
		pieceAtPositionImMovingTo = me.pieceAtPosition(parseFileIntoIndex(userInput.charAt(3)), Character.getNumericValue(userInput.charAt(4)));
		
		// One of my own pieces is already at the position provided making this invalid
		if(pieceAtPositionImMovingTo != null) {
			return false;
		}
		
		// Simulate moving the piece - see if the move results in check, if so, it is an invalid move
		Player meSim = new Player(me);
		Player themSim = new Player(them);
		actuallyHandleMove(meSim, themSim, userInput, false);
		if(meSim.checkForCheck(themSim)) {
			System.out.println(userInput);
			return false;
		}
		
		char promotionCode = userInput.length() == 7 ? userInput.charAt(6) : ' ';
		
		return pieceToMove.canMoveToNewPosition(parseFileIntoIndex(userInput.charAt(3)), Character.getNumericValue(userInput.charAt(4)), promotionCode, me, them);
	}
}
